#include "mining.h"
#include <iostream>
#include <sstream>
#include <cmath>
using namespace std;

void findGold() {
    int x=1001, y=1001;
    for(int i=1; i<=1000 && x>=1000 && y>=1000; i++){
        if(isIntegerDistance(i,1)){
            (isIntegerDistance(i,10)&&isIntegerDistance(i,100)) ? x=i : 1;
            (i==1000&& isIntegerDistance(500,1)&& isIntegerDistance(100,1))? y=1 : 1;
        }
    }
    for(int i=1; i<=1000 && (x>=1000 || y>=1000); i++){
        if(isIntegerDistance(1,i)){
            (isIntegerDistance(10,i)&&isIntegerDistance(100,i)) ? y=i : 1;
            (i==1000&& isIntegerDistance(1,500)&& isIntegerDistance(1,100))? x=1 : 1;
        }
    }
    answer(x,y);
}

//Partially solved: 50/100
//Original problem: https://competition.ia-toki.org/contests/270/problems/2010/