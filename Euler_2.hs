-- sum of all even Fibonacci number (starting with 1 and 2) below n
fib :: [Integer] 
fib = 1 : 2 : zipWith (+) fib (tail fib)

sumEvenFib :: Integer -> Integer 
sumEvenFib n = sum [ x | x <- filter even (takeWhile (<= n) fib)]
