#include "tetris.h"

#include <string>
#include <vector>
#include <algorithm>
using namespace std;

std::vector<int> arrangeTetrominoes(int N, std::string S, int Q) {
    vector<int> ans;
    if(!S.size()||(count(S.begin(),S.end(), 'J') + count(S.begin(), S.end(), 'L'))%2){
        return {};
    }
    else if (!Q){
        return vector<int> (N,0);
    }
    else {
        int j=0;
        while(S.size()){
            if (S.at(0)=='O'){
                ans.push_back(0);
            }else{
                ans.push_back(j%2? 3:1);
                j++;
            }
            S.erase(S.begin());
        }
        return ans;
    }
}

// Partially solved: 3/100
// Original Problem: https://competition.ia-toki.org/contests/272/problems/2020/