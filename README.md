Solution to various programming challanges, mainly on [Project Euler](https://projecteuler.net/)
and [CodeForces](https://codeforces.com/). Links to the original CodeForces problems are at the end of each file. 

H-99 is a series [ninety-nine Haskell problems](https://wiki.haskell.org/H-99:_Ninety-Nine_Haskell_Problems) 
I'm currently working through to get familiar with the language. In the future, I espect to do some CodeForces
problems in Haskell as well, rather than the usual C++.