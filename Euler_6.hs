sumSquare :: Integer -> Integer 
sumSquare n = sum [ x | x <- map (^2) [1..n]]

squareSum :: Integer -> Integer
squareSum n = (sum[1..n]) ^ 2

difference :: Integer -> Integer
difference n = squareSum n - sumSquare n
