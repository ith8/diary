#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;

int main()
{
    long long int n, k;
    cin >> n >> k;
    vector<long long int> a(n);
    for (int i=0; i<n; i++){
        cin >> a.at(i);
    }
    sort(a.begin(), a.end());
    long long int m=n-1, cnt=1;
    for (int i=0; i< m ; i++){
        (i && a.at(i-1)== a.at(i)) ? cnt ++ : cnt=1;
        if (a.at(i+1) != a.at(i) && a.at(i+1) <= a.at(i)+k) {
            n-=cnt ;
        }
    }
    cout << n;
}

// solved: time-312ms, memory-1572KB
// original problem: http://codeforces.com/contest/990/problem/B