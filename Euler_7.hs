-- naive prime gen
factors :: Integer -> [Integer]
factors n = [x | x <- [2..n-1], n `mod` x == 0]

prime :: Integer -> Bool
prime n = factors n == []

primes :: [Integer]
primes = [x | x <- [2..], prime x]

-- primeFac :: Integer -> [Integer]
-- primeFac n = [ x | x <- takeWhile (<= 775146 ) primes, n `mod` x == 0] 

