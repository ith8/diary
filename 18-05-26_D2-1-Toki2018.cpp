#include "tile.h"

#include <iostream>
#include <vector>

using namespace std;
int  k;
vector<int>  n1;

void init(int N, int K, int Q, int M, std::vector<int> A) {
    k=K;
    n1.assign(N,1);
    for(int i=0; i<M; i++){
        n1.at(A.at(i)-1)=0;
    }
}

int getNumberOfSpecialTiles(int L, int R) {
    int tileno=0;
    for(int i=L-1; i<R;){
        if(!n1.at(i)){
            tileno++;
            i+=k;
        }else{
            i++;
        }
    }
    return tileno;
}

// Partially solved: 15/100
// Original problem: https://competition.ia-toki.org/contests/272/problems/2016/