#include <iostream>

using namespace std;

int main()
{
    string s2;
    getline(cin, s2);
    while (s2==string(s2.rbegin(), s2.rend())&&s2.size()){
        s2.pop_back();
    }
    cout<<s2.size();
    return 0;
}

// Soloved: Time-15ms, Memory 0KB
// Original Problem: http://codeforces.com/contest/981/problem/A