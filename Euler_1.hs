-- find sum of all multiples of 3 or 5 below n.
mysum :: Integer -> Integer 
mysum n = sum [x | x <- [1..n-1], x `mod` 3 == 0 || x `mod` 5 == 0]
