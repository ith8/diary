#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int countSubsets(int M, int N, int K, std::vector<int> C, std::vector<int> D) {
    int cnt=0;
    vector <int> table;
    for(int i=1; i<=M; i++){ //1.get table
        for(int j=1; j<=N; j++){
            int k=1;
            while(k && k <= K){ //2. check for left out cells
                (i==C.at(k-1)&&j==D.at(k-1)) ? k=0: k++;
            }
            if (k){
                table.push_back(pow(i, j));
            }
        }
    }
    for(int i=0; i<pow(2,M*N-K);i++){// 3.check every combination
        int sum=0;
        int i2=i;
        for (int j=0; i2; j++){
            i2%2? sum+=table.at(j) : 1;
            i2/=2;
        }
        (sum%100 == 13)? cnt ++ : 1;
    }
    return cnt;
}

// Partially solved: 11/100
// Original problem: https://competition.ia-toki.org/contests/270/problems/2013/