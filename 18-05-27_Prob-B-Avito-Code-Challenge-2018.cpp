#include <iostream>
#include <sstream>
#include<algorithm>
#include<cmath>
#include<vector>

using namespace std;

int main()
{
    int l;
    long long int prof=0;
    cin>> l;
    vector <int> Ca(l), Cx(l);
    for(int i=0; i<l; i++){
        cin>>Ca.at(i) >> Cx.at(i);
        prof+= Cx.at(i);
    }
    cin>> l;
    vector <int> Ba(l), Bx(l);
    for(int i=0; i<l; i++){
        cin>>Ba.at(i) >> Bx.at(i);
        std::vector<int>::iterator it;
        it = find (Ca.begin(), Ca.end(), Ba.at(i));
        if (it != Ca.end()){
            prof+=(Bx.at(i)>Cx.at(it-Ca.begin()))*(Bx.at(i)-Cx.at(it-Ca.begin()));
        }else{
            prof+=Bx.at(i);
        }
    }
    cout<< prof;
    return 0;
}

// Partially Solved: Time limit exceeded
// Original Problem: http://codeforces.com/contest/981/problem/B