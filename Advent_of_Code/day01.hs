-- Find the sum of all digits that match the next digit in the list. The list is circular, so the digit after the last digit is the first digit in the list.

sumMatch :: [Integer] -> Integer
sumMatch xs = sum [x | (x,y) <- zip xs ((last xs):(init xs)), x == y]

int2ints :: Integer -> [Integer]
int2ints n
  | n == 0 = []
  | otherwise = (int2ints (n `div` 10)) ++ [n `mod` 10]

captcha :: Integer -> Integer
captcha = sumMatch . int2ints

-- Now, instead of considering the next digit, it wants you to consider the digit halfway around the circular list. That is, if your list contains 10 items, only include a digit in your sum if the digit 10/2 = 5 steps forward matches it.
sumMatch' :: [Integer] -> Integer
sumMatch' xs = sum [ x | (x,y) <- zip xs ((drop n xs) ++ (take n xs)), x == y]
  where n = (length xs) `div` 2

captcha' :: Integer -> Integer 
captcha' = sumMatch' . int2ints
