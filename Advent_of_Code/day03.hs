-- part 1
type Size = Int
type Layer = Int

-- #nums per layer
ringSize :: Layer -> Size
ringSize n = 8*n

-- #nums upto layer
totalUpto :: Layer -> Int
totalUpto n = 1 + sum (map ringSize [0..n])

-- #find layer of
layer :: Int -> Layer
layer x = 1 + last (takeWhile (\n -> (totalUpto n) < x) [0..])

makeLayer :: Layer -> [Int]
makeLayer l = take (ringSize l) [n..]
  where n = (totalUpto (l-1)+1)

distance :: Int -> Int 
distance n = l +  abs((pos `mod` sides) - (sides `div` 2))
  where l = layer n
	ls = makeLayer l
	pos = 1+ length (takeWhile (/= n) ls)
	sides = l*2 -- length of each sides -1

-- part 2
seq :: Int -> Int 
seq 0 = 1
seq 1 = 1
seq n 
  | -- normal edge piece +4
  | -- corner value +2
  | -- last corner value +3
  | -- after corner value +3 
