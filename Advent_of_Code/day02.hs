-- For each row, determine the difference between the largest value and the smallest value; the checksum is the sum of all of these differences.
checksum :: [[Int]] -> Int
checksum xss = sum [(maximum xs) - (minimum xs) | xs <- xss]

-- The goal is to find the only two numbers in each row where one evenly divides the other - that is, where the result of the division operation is a whole number. They would like you to find those numbers on each line, divide them, and add up each line's result.
checksum' :: [[Int]] -> Int
checksum' xss = sum [x `div` y | xs <- xss, x <- xs, y <- xs, x > y && (x `mod` y) == 0]

--converting input
--  sed 't/,/][/g' input.txt | perl -pe 's/\r?\n/],[/'
--  then wrap everything around [[]]
