#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    long long int m, n , a, b;
    cin >> n >> m >> a >> b;
    cout << ((n%m)*b <= (m-n%m)*a ? (n%m)*b : (m-n%m)*a );
    return 0;
}

// solved: time-31ms, memory-4KB
// original problem: http://codeforces.com/contest/990/problem/A