#include <iostream>
#include<vector>
#include<algorithm>
#include<string>
#include<sstream>

using namespace std;

int main()
{
    vector <string> words;
    string str;
    int n;
    bool y=1;
    getline(cin,str);
    stringstream (str) >> n;
    getline(cin,str);
    words.push_back(str);

    for(int j=0; j<n && getline(cin,str)&&y;j++){

        for(int i=0; i<words.size()&&y; i++){

            y=(((words.at(i)).size()>=str.size()&&(words.at(i)).find(str)!=string::npos)
               ||((words.at(i)).size()<= str.size()&&str.find(words.at(i))!=string::npos));
        }
        int i=0;
        for(; i<words.size()&&y&&str.size()>words.at(i).size(); i++){}
        words.insert(words.begin()+i,str);

    }
    cout<< (y? "YES": "NO");
    for(int i=0; i<words.size() &&y; i++){
        cout<<endl<<words.at(i);
    }
    return 0;
}

// Solved: Time-31ms, Memory-3500KB
// Original Problem: http://codeforces.com/contest/988/problem/B