rdigits :: Integer -> [Integer]
rdigits 0 = []
rdigits n = ((n `mod` 10):(rdigits (n `div` 10)))

facSum :: Integer -> Integer 
facSum n = sum (rdigits (product [1..n]))
