#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
    int i=1, n, k, temp;
    vector<int> cnt;
    string ans="";
    cin >> n >> k;
    while (i<=n&& cnt.size()<k){
        cin >> temp;
        if(find (cnt.begin(), cnt.end(), temp)==cnt.end()){
            cnt.push_back(temp);
            ans+=to_string(i) +" ";
        }
        i++;
    }
    if(cnt.size()==k)
        cout<< "YES"<< endl<< ans;
    else
        cout<< "NO";
    return 0;
}

// Solved: Time-30ms, Memory-3400KB
// Original Problem: http://codeforces.com/contest/988/problem/A